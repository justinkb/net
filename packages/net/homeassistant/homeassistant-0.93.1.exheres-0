# Copyright 2016-2019 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require pypi setup-py [ import=setuptools has_bin=true multibuild=false blacklist="2" test=pytest ] \
    systemd-service

SUMMARY="Open-source home automation platform running on Python 3"
DESCRIPTION="
The goal of Home Assistant is to be able to track and control all devices at home and offer a
platform for automating control.
"
HOMEPAGE+=" https://home-assistant.io"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

# Need to figure out
RESTRICT="test"

DEPENDENCIES="
    build+run:
        group/${PN}
        user/${PN}
        dev-python/Jinja2[>=2.10][python_abis:*(-)?]
        dev-python/PyJWT[>=1.7.1][python_abis:*(-)?]
        dev-python/PyYAML[>=3.13&<4][python_abis:*(-)?]
        dev-python/aiohttp[>=3.5.4][python_abis:*(-)?]
        dev-python/astral[>=1.10.1][python_abis:*(-)?]
        dev-python/async-timeout[>=3.0.1][python_abis:*(-)?]
        dev-python/attrs[>=19.1.0][python_abis:*(-)?]
        dev-python/bcrypt[>=3.1.6][python_abis:*(-)?]
        dev-python/certifi[>=2018.4.16][python_abis:*(-)?]
        dev-python/cryptography[>=2.6.1][python_abis:*(-)?]
        dev-python/pip[>=8.0.3][python_abis:*(-)?]
        dev-python/python-slugify[>=3.0.2][python_abis:*(-)?]
        dev-python/pytz[>=2019.1][python_abis:*(-)?]
        dev-python/requests[>=2.21.0&<3][python_abis:*(-)?]
        dev-python/ruamel-yaml[>=0.15.94][python_abis:*(-)?]
        dev-python/voluptuous[>=0.11.5][python_abis:*(-)?]
        dev-python/voluptuous-serialize[>=2.1.0][python_abis:*(-)?]
    run:
        dev-python/PyQRCode[>=1.2.1][python_abis:*(-)?]
        dev-python/SQLAlchemy[>=1.3.3][python_abis:*(-)?]
        dev-python/aiohttp-cors[>=0.7.0][python_abis:*(-)?]
        dev-python/colorlog[>=4.0.2][python_abis:*(-)?]
        dev-python/distro[>=1.4.0][python_abis:*(-)?]
        dev-python/home-assistant-frontend[>=20190514.0][python_abis:*(-)?]
        dev-python/netdisco[>=2.6.0][python_abis:*(-)?]
        dev-python/pyotp[>=2.2.7][python_abis:*(-)?]
        dev-python/xmltodict[>=0.12.0][python_abis:*(-)?]
    suggestion:
        app-speech/svox [[
            description = [ Required for Home Assistant component: tts.picotts ]
        ]]
        dev-python/PyChromecast[>=3.2.1][python_abis:*(-)?] [[
            description = [ Required for Home Assistant component: cast ]
        ]]
        dev-python/pyHS100[>=0.3.5][python_abis:*(-)?] [[
            description = [ Required for Home Assistant component: tplink ]
        ]]
        dev-python/beautifulsoup4[>=4.7.1][python_abis:*(-)?] [[
            description = [ Required for Home Assistant component: linksys_ap, scrape, sytadin ]
        ]]
        dev-python/gstreamer-player[>=1.1.2][python_abis:*(-)?] [[
            description = [ Required for Home Assistant component: gstreamer ]
        ]]
        dev-python/jsonrpc-async[>=0.6][python_abis:*(-)?] [[
            description = [ Required for Home Assistant component: kodi ]
        ]]
        dev-python/jsonrpc-websocket[>=0.6][python_abis:*(-)?] [[
            description = [ Required for Home Assistant component: kodi ]
        ]]
        dev-python/limitlessled[>=1.1.3][python_abis:*(-)?] [[
            description = [ Required for Home Assistant component: limitlessled ]
        ]]
        dev-python/mutagen[>=1.42.0][python_abis:*(-)?] [[
            description = [ Required for Home Assistant component: tts ]
        ]]
        dev-python/onkyo-eiscp[>=1.2.4][python_abis:*(-)?] [[
            description = [ Required for Home Assistant component: onkyo ]
        ]]
        dev-python/paho-mqtt[>=1.4.0][python_abis:*(-)?] [[
            description = [ Required for Home Assistant component: mqtt, shiftr ]
        ]]
        dev-python/psutil[>=5.6.2][python_abis:*(-)?] [[
            description = [ Required for Home Assistant component: systemmonitor ]
        ]]
        dev-python/pyhomematic[>=0.1.58][python_abis:*(-)?] [[
            description = [ Required for Home Assistant component: homematic ]
        ]]
        dev-python/pylast[>=3.1.0][python_abis:*(-)?] [[
            description = [ Required for Home Assistant component: lastfm ]
        ]]
        dev-python/python-mpd2[>=1.0.0][python_abis:*(-)?] [[
            description = [ Required for Home Assistant component: mpd ]
        ]]
        dev-python/python-nmap[>=0.6.1][python_abis:*(-)?] [[
            description = [ Required for Home Assistant component: nmap_tracker ]
        ]]
        dev-python/pywebpush[>=1.9.2][python_abis:*(-)?] [[
            description = [ Required for Home Assistant component: html5 ]
        ]]
        dev-python/snapcast[>=2.0.9][python_abis:*(-)?] [[
            description = [ Required for Home Assistant component: snapcast ]
        ]]
        dev-python/spotipy[>=2.4.4][python_abis:*(-)?] [[
            description = [ Required for Home Assistant component: spotify ]
        ]]
        dev-python/wakeonlan[>=1.1.6][python_abis:*(-)?] [[
            description = [ Required for Home Assistant component: wake_on_lan, panasonic_viera, samsungtv, wake_on_lan ]
        ]]
        dev-python/websockets[>=6.0][python_abis:*(-)?] [[
            description = [ Required for Home Assistant component: webostv ]
        ]]
        dev-python/zeroconf[>=0.22.0][python_abis:*(-)?] [[
            description = [ Required for Home Assistant component: zeroconf ]
        ]]
        sys-apps/net-tools [[
            description = [ Required for Home Assistant component: nmap_tracker ]
        ]]
        net-misc/iputils [[
            description = [ Required for Home Assistant component: wake_on_lan ]
        ]]
        net-misc/youtube-dl[>=2019.04.30][python_abis:*(-)?] [[
            description = [ Required for Home Assistant component: media_extractor ]
        ]]
"

src_prepare() {
    setup-py_src_prepare

    # can be more recent
    edo sed \
        -e 's:aiohttp==3.5.4:aiohttp>=3.5.4:g' \
        -e 's:astral==1.10.1:astral>=1.10.1:g' \
        -e 's:async_timeout==3.0.1:async_timeout>=3.0.1:g' \
        -e 's:attrs==19.1.0:attrs>=19.1.0:g' \
        -e 's:bcrypt==3.1.6:bcrypt>=3.1.6:g' \
        -e 's:PyJWT==1.7.1:PyJWT>=1.7.1:g' \
        -e 's:cryptography==2.6.1:cryptography>=2.6.1:g' \
        -e 's:python-slugify==3.0.2:python-slugify>=3.0.2:g' \
        -e 's:requests==2.21.0:requests>=2.21.0:g' \
        -e 's:ruamel.yaml==0.15.94:ruamel.yaml>=0.15.94:g' \
        -e 's:voluptuous==0.11.5:voluptuous>=0.11.5:g' \
        -e 's:voluptuous-serialize==2.1.0:voluptuous-serialize>=2.1.0:g' \
        -i homeassistant/package_constraints.txt \
        -i setup.py

    # Core components
    # homeassistant.auth.mfa_modules.totp
    edo sed \
        -e 's:PyQRCode==1.2.1:PyQRCode>=1.2.1:g' \
        -i homeassistant/auth/mfa_modules/totp.py
    # homeassistant.components.recorder
    # homeassistant.components.sql
    edo sed \
        -e 's:sqlalchemy==1.3.3:sqlalchemy>=1.3.3:g' \
        -i homeassistant/components/{recorder,sql}/manifest.json
    # homeassistant.components.emulated_hue
    # homeassistant.components.http
    edo sed \
        -e 's:aiohttp_cors==0.7.0:aiohttp_cors>=0.7.0:g' \
        -i homeassistant/components/{emulated_hue,http}/manifest.json
    # homeassistant.scripts.check_config
    edo sed \
        -e 's:colorlog==4.0.2:colorlog>=4.0.2:g' \
        -i homeassistant/scripts/check_config.py
    # homeassistant.components.updater
    edo sed \
        -e 's:distro==1.4.0:distro>=1.4.0:g' \
        -i homeassistant/components/updater/manifest.json
    # homeassistant.components.frontend
    edo sed \
        -e 's:home-assistant-frontend==20190514.0:home-assistant-frontend>=20190514.0:g' \
        -i homeassistant/components/frontend/manifest.json
    # homeassistant.components.discovery
    edo sed \
        -e 's:netdisco==2.6.0:netdisco>=2.6.0:g' \
        -i homeassistant/components/discovery/manifest.json
    # homeassistant.auth.mfa_modules.notify
    # homeassistant.auth.mfa_modules.totp
    # homeassistant.components.otp
    edo sed \
        -e 's:pyotp==2.2.7:pyotp>=2.2.7:g' \
        -i homeassistant/auth/mfa_modules/{notify,totp}.py \
        -i homeassistant/components/otp/manifest.json
    # homeassistant.components.bluesound
    # homeassistant.components.startca
    # homeassistant.components.ted5000
    # homeassistant.components.yr
    # homeassistant.components.zestimate
    edo sed \
        -e 's:xmltodict==0.12.0:xmltodict>=0.12.0:g' \
        -i homeassistant/components/{bluesound,startca,ted5000,yr,zestimate}/manifest.json


    # Additional components
    # homeassistant.components.cast
    edo sed \
        -e 's:pychromecast==3.2.1:pychromecast>=3.2.1:g' \
        -i homeassistant/components/cast/manifest.json
    # homeassistant.components.html5
    edo sed \
        -e 's:pywebpush==1.9.2:pywebpush>=1.9.2:g' \
        -i homeassistant/components/html5/manifest.json
    # homeassistant.components.tplink
    edo sed \
        -e 's:pyHS100==0.3.5:pyHS100>=0.3.5:g' \
        -i homeassistant/components/tplink/manifest.json
    # homeassistant.components.linksys_ap
    # homeassistant.components.scrape
    # homeassistant.components.sytadin
    edo sed \
        -e 's:beautifulsoup4==4.7.1:beautifulsoup4>=4.7.1:g' \
        -i homeassistant/components/{linksys_ap,scrape,sytadin}/manifest.json
    # homeassistant.components.gstreamer
    edo sed \
        -e 's:gstreamer-player==1.1.2:gstreamer-player>=1.1.2:g' \
        -i homeassistant/components/gstreamer/manifest.json
    # homeassistant.components.kodi
    edo sed \
        -e 's:jsonrpc-async==0.6:jsonrpc-async>=0.6:g' \
        -e 's:jsonrpc-websocket==0.6:jsonrpc-websocket>=0.6:g' \
        -i homeassistant/components/kodi/manifest.json
    # homeassistant.components.limitlessled
    edo sed \
        -e 's:limitlessled==1.1.3:limitlessled>=1.1.3:g' \
        -i homeassistant/components/limitlessled/manifest.json
    # homeassistant.components.tts
    edo sed \
        -e 's:mutagen==1.42.0:mutagen>=1.42.0:g' \
        -i homeassistant/components/tts/manifest.json
    # homeassistant.components.onkyo
    edo sed \
        -e 's:onkyo-eiscp==1.2.4:onkyo-eiscp>=1.2.4:g' \
        -i homeassistant/components/onkyo/manifest.json
    # homeassistant.components.mqtt
    # homeassistant.components.shiftr
    edo sed \
        -e 's:paho-mqtt==1.4.0:paho-mqtt>=1.4.0:g' \
        -i homeassistant/components/{mqtt,shiftr}/manifest.json
    # homeassistant.components.systemmonitor
    edo sed \
        -e 's:psutil==5.6.2:psutil>=5.6.2:g' \
        -i homeassistant/components/systemmonitor/manifest.json
    # homeassistant.components.homematic
    edo sed \
        -e 's:pyhomematic==0.1.58:pyhomematic>=0.1.58:g' \
        -i homeassistant/components/homematic/manifest.json
    # homeassistant.components.lastfm
    edo sed \
        -e 's:pylast==3.1.0:pylast>=3.1.0:g' \
        -i homeassistant/components/lastfm/manifest.json
    # homeassistant.components.mpd
    edo sed \
        -e 's:python-mpd2==1.0.0:python-mpd2>=1.0.0:g' \
        -i homeassistant/components/mpd/manifest.json
    # homeassistant.components.nmap_tracker
    edo sed \
        -e 's:python-nmap==0.6.1:python-nmap>=0.6.1:g' \
        -i homeassistant/components/nmap_tracker/manifest.json
    # homeassistant.components.snapcast
    edo sed \
        -e 's:snapcast==2.0.9:snapcast>=2.0.9:g' \
        -i homeassistant/components/snapcast/manifest.json
    # homeassistant.components.spotify
    edo sed \
        -e 's:spotipy-homeassistant==2.4.4.dev1:spotipy>=2.4.4:g' \
        -i homeassistant/components/spotify/manifest.json
    # homeassistant.components.panasonic_viera
    # homeassistant.components.samsungtv
    # homeassistant.components.wake_on_lan
    edo sed \
        -e 's:wakeonlan==1.1.6:wakeonlan>=1.1.6:g' \
        -i homeassistant/components/{panasonic_viera,samsungtv,wake_on_lan}/manifest.json
    # homeassistant.components.webostv
    edo sed \
        -e 's:websockets==6.0:websockets>=6.0:g' \
        -i homeassistant/components/webostv/manifest.json
    # homeassistant.components.zeroconf
    edo sed \
        -e 's:zeroconf==0.22.0:zeroconf>=0.22.0:g' \
        -i homeassistant/components/zeroconf/manifest.json
    # homeassistant.components.media_extractor
    edo sed \
        -e 's:youtube_dl==2019.04.30:youtube_dl>=2019.04.30:g' \
        -i homeassistant/components/media_extractor/manifest.json
}

src_install() {
    setup-py_src_install

    keepdir /var/lib/homeassistant
    edo chown homeassistant:homeassistant "${IMAGE}"/var/lib/homeassistant

    install_systemd_files
}

