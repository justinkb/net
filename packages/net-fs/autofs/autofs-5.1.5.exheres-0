# Copyright 2010 Timothy Redaelli <timothy@redaelli.eu>
# Copyright 2010 Mike Kelly
# Copyright 2018 Rasmus Thomsen <cogitri@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]
require openrc-service systemd-service
require option-renames [ renames=[ 'openldap ldap' ] ]

SUMMARY="Kernel based automounter"

DOWNLOADS="mirror://kernel/linux/daemons/${PN}/v$(ever major)/${PNV}.tar.xz"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"

MYOPTIONS="
    ldap
    sasl [[ requires = openldap ]]
    systemd
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        net-libs/libtirpc
        sys-apps/util-linux [[ note = [ for mount utils ] ]]
        ldap? ( net-directory/openldap )
        sasl? (
            app-crypt/krb5
            dev-libs/libxml2:2.0
        )
        systemd? ( sys-apps/systemd )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-fedfs
    --enable-force-shutdown
    --enable-no-canon-umount
    --enable-ignore-busy
    --disable-mount-locking
    --with-confdir=/etc/autofs
    --with-libtirpc
)

DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    'ldap openldap'
    sasl
    systemd
)

DEFAULT_SRC_COMPILE_PARAMS=( DONTSTRIP=1 )

DEFAULT_SRC_INSTALL_PARAMS=( systemddir="${SYSTEMDSYSTEMUNITDIR}" install_samples )

src_prepare() {
    # Wants to use cpp directly otherwise
    edo sed -i 's:^RPCGEN = .*$:& -Y /usr/bin:' Makefile.conf.in

    autotools_src_prepare
}

src_install() {
    default

    # Upstream's rc file doesn't start on boot with openrc
    install_openrc_files

    # remove empty dir
    edo rmdir "${IMAGE}"/run
}

