# Copyright 2009-2012 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'cyrus-imapd-2.3.13-r1.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation

require multibuild autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.10 ] ] pam systemd-service

export_exlib_phases src_prepare src_install pkg_postinst

SUMMARY="The Cyrus IMAP (Internet Message Access Protocol) server"
DESCRIPTION="
The Cyrus IMAP server is generally intended to be run on sealed systems, where
normal users are not permitted to log in. The mailbox database is stored in parts
of the filesystem that are private to Cyrus. All user access to mail is through
the IMAP, POP3, or KPOP protocols. The private mailbox database design gives the
server large advantages in efficiency, scalability, and administratability. Multiple
concurrent read/write connections to the same mailbox are permitted. The server
supports access control lists on mailboxes and storage quotas on mailbox hierarchies,
multiple SASL mechanisms, and the Sieve mail filtering language.
"
HOMEPAGE="http://www.cyrusimap.org/"
DOWNLOADS="ftp://ftp.cyrusimap.org/${PN}/${PNV}.tar.gz"

BUGS_TO=""
REMOTE_IDS="freecode:cyrusimapserver"

UPSTREAM_CHANGELOG="https://bugzilla.andrew.cmu.edu:443/cgi-bin/cvsweb.cgi/~checkout~/src/cyrus/doc/changes.html"
UPSTREAM_DOCUMENTATION="${HOMEPAGE}/imapd/ [[ lang = en ]]"

LICENCES="cyrus-sasl"
SLOT="0"
MYOPTIONS="
    idled [[ description = [ idled listens for change notifications and alerts imapd/popd. If unset, polling is used instead. ] ]]
    kerberos
    pam
    perl [[ description = [ Provides a Perl interface to the Cyrus imclient library ] ]]
    replication [[ description = [ Allows for replicating the mailstore to a secondary server (load-balancing not yet supported) ] ]]
    sieve [[ description = [ Enable Sieve (RFC 5228) server-side message processing/filtering ] ]]
    snmp
    ssl
    tcpd
    platform: amd64
"

DEPENDENCIES="
    build+run:
        group/mail
        user/cyrus
        user/mail
        user/postmaster
        net-libs/cyrus-sasl[>=2.1.22]
        sys-fs/e2fsprogs
        sys-libs/db:=[>=4.6.21]
        kerberos? ( app-crypt/heimdal )
        pam? ( sys-libs/pam[>=1.0.4] )
        perl? ( dev-lang/perl:*[>=5.8.9] )
        snmp? ( net-analyzer/net-snmp[tcpd=] )
        ssl? ( virtual/libssl )
        tcpd? ( sys-apps/tcp-wrappers[>=7.6] )
"
# nntp-over-imap is an abomination I'm not going to add.

DEFAULT_SRC_PREPARE_PATCHES=( "${FILES}"/${PN}-strip.patch )

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-murder
    --enable-netscapehack
    --disable-nntp
    --disable-oldsievename
    --with-cyrus-user=cyrus
    --with-cyrus-group=mail
    --with-com_err=""
    --with-extraident=Exherbo
    --with-service-path=/usr/${LIBDIR}/cyrus
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    idled
    "kerberos gssapi"
    "kerberos krb5afspts"
    replication
    sieve
)

DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    perl
    "snmp ucdsnmp"
    "ssl openssl"
    "tcpd libwrap"
)

DEFAULT_SRC_COMPILE_PARAMS=( -j1 )

AT_M4DIR=( "cmulocal" )

cyrus-imapd_src_prepare() {
    default

    # Fix master(8)->cyrusmaster(8) manpage.
    for i in $(grep -rl -e 'master\.8' -e 'master(8)' "${WORK}") ; do
        edo sed -i -e 's:master\.8:cyrusmaster.8:g' \
                   -e 's:master(8):cyrusmaster(8):g' "${i}"
    done
    edo mv man/{,cyrus}master.8
    edo sed -e "s:MASTER:CYRUSMASTER:g" \
            -e "s:Master:Cyrusmaster:g" \
            -e "s:master:cyrusmaster:g" \
            -i man/cyrusmaster.8

    if option platform:amd64 ; then
        edo sed -i -e "s:^\(CFLAGS = .*\):\1 -fPIC:" lib/Makefile.in
        edo sed -i -e "s:^\(CFLAGS = .*\):\1 -fPIC:" perl/sieve/lib/Makefile.in
    fi

    # Avoid spamming the syslog.
    edo sed -i -e "/fetching user_deny.db/d" imap/userdeny_db.c

    eautoreconf
}

cyrus-imapd_src_install() {
    default

    # Link master to cyrusmaster (postfix has a master, too)
    dosym /usr/${LIBDIR}/cyrus/master /usr/${LIBDIR}/cyrus/cyrusmaster

    edo rm \
        man/fetchnews.8 \
        man/syncnews.8 \
        man/nntpd.8 \
        man/nntptest.1 \
        "${IMAGE}"/usr/bin/nntptest

    doman man/*.[0-8]

    docinto html
    dodoc \
        doc/*.html \
        doc/murder.png \
        doc/cyrusv2.mc
    docinto text
    dodoc doc/text/*
    edo cp -r contrib tools "${IMAGE}"/usr/share/doc/${PNVR}
    edo find "${IMAGE}"/usr/share/doc -name CVS -print0 | xargs -0 rm -rf

    insinto /etc
    doins "${FILES}"/{cyrus,imapd}.conf

    option pam && newpamd "${FILES}"/${PN}.pam sieve

    install_systemd_files
    # Set the correct libdir in the service.
    option systemd && edo sed -i -e "s:LIBDIR:${LIBDIR}:" "${IMAGE}"/${SYSTEMDSYSTEMUNITDIR}/cyrus.service

    for subdir in imap/{,db,log,msg,proc,socket,sieve} spool/imap/{,stage.} ; do
        keepdir "/var/${subdir}"
        edo chown cyrus:mail "${IMAGE}/var/${subdir}"
        edo chmod 0750 "${IMAGE}/var/${subdir}"
    done
    for subdir in imap/{user,quota,sieve} spool/imap ; do
        for i in a b c d e f g h i j k l m n o p q r s t v u w x y z ; do
            keepdir "/var/${subdir}/${i}"
            edo chown cyrus:mail "${IMAGE}/var/${subdir}/${i}"
            edo chmod 0750 "${IMAGE}/var/${subdir}/${i}"
        done
    done

    edo find "${IMAGE}"/usr -type d -empty -delete
}

cyrus-imapd_pkg_postinst() {
    if df -T /var/imap | grep -q ' ext2 ' ; then
        ebegin "Making /var/imap/user/* and /var/imap/quota/* synchronous."
        chattr +S /var/imap/{user,quota}{,/*}
        eend $?
    fi

    if df -T /var/spool/imap | grep -q ' ext2 ' ; then
        ebegin "Making /var/spool/imap/* synchronous."
        chattr +S /var/spool/imap{,/*}
        eend $?
    fi

    ewarn "If the queue directory of the mail daemon resides on an ext2"
    ewarn "filesystem you need to set it manually to update"
    ewarn "synchronously. E.g. 'chattr +S /var/spool/mqueue'."
    echo

    elog "For correct logging, add something like the following to your syslogger's configuration:"
    elog "    local6.*         /var/log/imapd.log"
    elog "    auth.debug       /var/log/auth.log"
    echo

    elog "You have to add the cyrus user to the sasl database:"
    elog "    saslpasswd2 cyrus"
}

