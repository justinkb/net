# Copyright 2017-2019 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=greenbone tag=v${PV} ] \
    cmake [ api=2 ] \
    systemd-service

SUMMARY="Greenbone Vulnerability Manager"
HOMEPAGE+=" http://www.openvas.org"

LICENCES="GPL-2 LGPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-libs/libxslt
        virtual/pkg-config
    build+run:
        app-crypt/gpgme
        dev-db/sqlite:3[>=3.8.3]
        dev-libs/glib:2[>=2.42]
        dev-libs/gnutls[>=3.2.15]
        net-analyzer/gvm-libs[>=10.0.0]
        office-libs/libical:=[>=1.00]
    run:
        net-analyzer/openvas-scanner[>=6.0.0]
    suggestion:
        dev-texlive/texlive-latex [[
            description = [ Required for exporting scan reports as PDF ]
        ]]
        dev-texlive/texlive-latexextra [[
            description = [ Required for exporting scan reports as PDF ]
        ]]
    test:
        dev-util/cppcheck
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBACKEND:STRING=SQLITE3
    -DCLANG_FORMAT:BOOL=FALSE
    -DCMAKE_BUILD_TYPE:STRING=Release
    -DCMAKE_DISABLE_FIND_PACKAGE_Git:BOOL=TRUE
    -DCMAKE_INSTALL_DOCDIR:PATH=/usr/share/doc/${PNVR}
    -DDATADIR:PATH=/usr/share
    -DDEBUG_FUNCTION_NAMES:BOOL=FALSE
    -DDOXYGEN_EXECUTABLE:BOOL=FALSE
    -DENABLE_COVERAGE:BOOL=FALSE
    -DGVM_RUN_DIR:PATH=/run
    -DLIBDIR:PATH=/usr/$(exhost --target)/lib
    -DLOCALSTATEDIR:PATH=/var
    -DSBINDIR:PATH=/usr/$(exhost --target)/bin
    -DSYSCONFDIR:PATH=/etc
    -DXMLMANTOHTML_EXECUTABLE:BOOL=FALSE
    -DXMLTOMAN_EXECUTABLE:BOOL=FALSE
)

src_prepare() {
    cmake_src_prepare

    # TODO: report upstream
    edo sed \
        -e 's:DESTINATION ${DATADIR}/doc/gvm:DESTINATION ${CMAKE_INSTALL_DOCDIR}:g' \
        -i CMakeLists.txt
    edo sed \
        -e 's:DESTINATION share/doc/gvm:DESTINATION ${CMAKE_INSTALL_DOCDIR}:g' \
        -i doc/CMakeLists.txt
    edo sed \
        -e 's:DESTINATION share:DESTINATION ${CMAKE_INSTALL_DATAROOTDIR}:g' \
        -i doc/CMakeLists.txt
}

src_install() {
    cmake_src_install

    install_systemd_files

    keepdir /var/{lib,log}/gvm
    keepdir /var/lib/gvm/gvmd
}

