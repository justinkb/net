# Copyright 2017-2019 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=greenbone project=${PN/-scanner} tag=v${PV} ] \
    cmake [ api=2 ] \
    systemd-service

SUMMARY="OpenVAS scanner"
HOMEPAGE+=" http://www.openvas.org"

LICENCES="GPL-2 public-domain"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    snmp
"

DEPENDENCIES="
    build:
        sys-devel/bison
        virtual/pkg-config
    build+run:
        app-crypt/gpgme[>=1.1.2]
        dev-libs/glib:2[>=2.42]
        dev-libs/gnutls[>=3.2.15]
        dev-libs/libgcrypt[>=1.6]
        dev-libs/libksba[>=1.0.7]
        dev-libs/libpcap
        net-libs/libssh[>=0.6.0]
        net-analyzer/gvm-libs[>=10.0.0]
        snmp? ( net/net-snmp )
    run:
        net-scanner/nmap [[ note = [ The predefined scan configurations needs nmap as a port scanner ] ]]
    run+test:
        dev-db/redis[>=2.6]
    test:
        dev-util/cppcheck
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DCLANG_FORMAT:BOOL=FALSE
    -DCMAKE_BUILD_TYPE:STRING=Release
    -DCMAKE_DISABLE_FIND_PACKAGE_Git:BOOL=TRUE
    -DDATADIR:PATH=/usr/share
    -DDOXYGEN_EXECUTABLE:BOOL=FALSE
    -DENABLE_COVERAGE:BOOL=FALSE
    -DLIBDIR:PATH=/usr/$(exhost --target)/lib
    -DLOCALSTATEDIR:PATH=/var
    -DOPENVAS_RUN_DIR:PATH=/run
    -DSBINDIR:PATH=/usr/$(exhost --target)/bin
    -DSYSCONFDIR:PATH=/etc
)
CMAKE_SRC_CONFIGURE_OPTIONS=(
    'snmp SNMP'
)

src_install() {
    cmake_src_install

    install_systemd_files

    insinto /etc/openvas
    hereins openvassd.conf << EOF
kb_location = /tmp/redis.sock
EOF

    keepdir /var/log/gvm

    # greenbone-nvt-sync
    keepdir /var/lib/openvas/plugins

    # remove empty directories
    edo rmdir "${IMAGE}"/etc/openvas/gnupg
    edo rmdir "${IMAGE}"/var/lib/openvas/gnupg
    edo rmdir "${IMAGE}"/usr/share/openvas
}

